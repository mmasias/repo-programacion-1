import java.util.Scanner;
public class elCaminante{

	public static void main(String[] args){

		Scanner entrada = new Scanner(System.in);

		String[][] elMundo = {
			{"MAR", "MONTE    ",   "MONTE    ",   "MONTE    ", "MONTE    ", "MONTE    ", "MONTE  "  },
			{"MAR", "BOSQUE   ",   "BOSQUE   ",   "COLINA   ", "RUINAS   ", "BOSQUE   ", "MONTE  "  },
			{"MAR", "CASA     ",   "BOSQUE   ",   "ARBOL    ", "LAGO     ", "LAGO     ", "MONTE  "  },
			{"MAR", "PLANICIE ",   "BOSQUE   ",   "POZO     ", "LAGO     ", "LAGO     ", "CUEVA  "  },
			{"MAR", "BOSQUE   ",   "BOSQUE   ",   "BOSQUE   ", "RUINAS   ", "BOSQUE   ", "MONTE  "  },
			{"MAR", "RISCO    ",   "RISCO    ",   "RISCO    ", "RISCO    ", "RISCO    ", "MONTE  "  },
		};

		String[][] elMundoDetallado= {
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Las monta�as son muy altas y bloquean el paso...",     "Las monta�as son muy altas y bloquean el paso...",     "Las monta�as son muy altas y bloquean el paso...",         "Las monta�as son muy altas y bloquean el paso...",     "Las monta�as son muy altas y bloquean el paso...",     "Las monta�as son muy altas y bloquean el paso..."},
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "La colina comanda la vista de esta zona.",           "Te encuentras ante ruinas muy antiguas.",       "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Las monta�as son muy altas y bloquean el paso..."},
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Hay una casa abandonada con todas las entradas cerradas",         "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Un misterioso arbol se yergue altivo en un claro.", "Est�s al noroeste de un lago muy grande.",         "Est�s al noreste de un lago muy grande.",         "Las monta�as son muy altas y bloquean el paso..."},
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Una extensa planicie sin nada m�s que cesped",     "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Un pozo solitario se ubica dentro del bosque",             "Est�s al suroeste de un lago muy grande.",         "Est�s al sureste de un lago muy grande.",         "Hay una cueva misteriosa que tiene bloqueada la entrada."},
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Los �rboles de bosque bloquean el paso de los rayos del sol.",           "Te encuentras ante ruinas muy antiguas.",       "Los �rboles de bosque bloquean el paso de los rayos del sol.",       "Las monta�as son muy altas y bloquean el paso..."},
			{"El mar se extiende al oeste mas alla de lo que alcanza la vista...", "Un profundo precipicio impide ir m�s hacia el sur",   "Un profundo precipicio impide ir m�s hacia el sur",   "Un profundo precipicio impide ir m�s hacia el sur",       "Un profundo precipicio impide ir m�s hacia el sur",   "Un profundo precipicio impide ir m�s hacia el sur",   "Las monta�as son muy altas y bloquean el paso..."},
		};

		int posX, posY;
		int minX, minY, maxX, maxY;

		// Punto de partida del jugador
		posX=1;  posY=1;

		// Limites del mundo
		minX=0; minY=0;
		maxY=elMundo.length-1;
		maxX=elMundo[0].length-1;

	  boolean caminando = true;
	  String accion;
	  
	  
		System.out.println ("--------------------------------------------------------------------");
		System.out.println ("Despiertas con la sensaci�n de estar perdido, en medio de un bosque.");
		System.out.println ("Aun mareado, empiezas a caminar...");
		System.out.println ("--------------------------------------------------------------------");
		
	  while(caminando==true){
			System.out.println();
			System.out.println("Ubicacion: "+elMundo[posY][posX]);
			System.out.println ("--------------------------------------------------------------------");
			System.out.println("Los comandos son 'norte', 'sur', 'este', 'oeste', 'mira' y 'fin'");
			System.out.print  ("Ingrese comando: ");

			accion = entrada.nextLine();
			System.out.println ("--------------------------------------------------------------------");

			
			if (accion.equals("norte")){

				 if(posY>minY){
					  posY=posY-1;
					  System.out.println (" | Caminas hacia el norte...");
				 } else {
					  System.out.println (" | No puedes subir m�s! ");
				 }

			}

			else if (accion.equals("sur")){
				 if(posY<maxY){
					  posY=posY+1;
					  System.out.println (" | Caminas hacia el sur...");
				 } else {
					  System.out.println (" | No puedes bajar m�s! ");
				 }
			}
			else if (accion.equals("este")){
				 if(posX<maxX){
					  posX=posX+1;
					  System.out.println (" | Caminas hacia el este...");
				 }else {
					  System.out.println (" | No puedes ir mas al este! ");
				 }                
			}
			else if (accion.equals("oeste")){
				 if(posX>minX){
					  posX=posX-1;
					  System.out.println (" | Caminas hacia el oeste...");
				 }else {
					  System.out.println (" | No puedes ir mas al oeste! ");
				 }                
			}
			else if (accion.equals("mira")) {
				 System.out.print(" | Mirando alrededor, ");
				 System.out.println(elMundoDetallado[posY][posX]);
			}
			else if (accion.equals("fin")){
				 caminando=false;
			}
			else { 
				 System.out.println("No le he entendido!");
			}
	  }
	}

}
